This application downloads and normalizes public data from a GitLab project into a local database
for further cleaning and use in fine-tuning LLMs.

## Setup

### Run the script

Run the setup script (as root): 

```sh
bin/setup
```

This will create a .env file called `.env.generated`. You can then source this:

```sh
source .env.generated
```


### Or

If that doesn't work, you can take the steps manually.

- `CREATE DATABASE ai_data`
- `CREATE USER ai_data WITH PASSWORD []`
- `\c ai_data`
- `GRANT ALL PRIVILEGES ON SCHEMA public TO ai_data`

Put the user and password into envvars called `PG_USER` and `PG_PASSWORD` and the database name in an envvar called `PG_DATABASE`.

Note: You can `cp .env.example .env` and put these into the file, then simply source it:

```sh
. .env
```

Then run the migrations:

```sh
bundle exec sequel -m db/migrations postgres://ai_data:$PG_PASSWORD@localhost/$PG_DATABASE
```

## Usage

Run the script:

```sh
bin/download_issues --project gitlab-org/plan --threads 5
```

Specifying a project is optional. Omitting the `--project` argument will use `gitlab-org/gitlab`.

Specifying the number of threads is optional. Omitting the `--threads` argument will use a single thread. Nothing is done to manage GitLab rate limits. If you set the number of threads too high you might lose data.

Doesn't work? Run the script directly:

```sh
bundle exec ruby download_issues.rb --project gitlab-org/plan --threads 5
```

## Confidentiality

This script is configured to only download public issues. As such, no access token
is required. Specifying an access token with elevated permissions won't change this.

To download non-public issues, a change must also be made to the code in `download_issues.rb` 
to specify that confidential issues are acceptable.
