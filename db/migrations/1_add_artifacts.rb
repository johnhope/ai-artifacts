Sequel.migration do
  up do
    create_table(:artifacts) do
      primary_key :id
      bigint :gitlab_id
      bigint :iid
      String :title
      Text :body
      String :web_url
      bigint :parent_id
      bigint :parent_iid
      String :parent_type
    end
  end

  down do
    drop_table(:artifacts)
  end
end

