require 'sequel'

Sequel.connect("postgres://#{ENV['PG_USER']}:#{ENV['PG_PASSWORD']}@localhost/#{ENV['PG_DATABASE']}")

require_relative 'models/artifact'
