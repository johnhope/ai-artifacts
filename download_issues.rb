require 'gitlab'
require 'optparse'
require_relative 'app'

Gitlab.configure do |config|
  config.endpoint = 'https://gitlab.com/api/v4'
  config.private_token = ''
end

option_parser = OptionParser.new do |opts|
  opts.banner = "Usage: ruby download_issues.rb [options]"

  opts.on("--project PROJECT", String, "Specify a project name (default gitlab-org/gitlab)") do |project|
    @project = project
  end

  opts.on("--threads THREADS", Integer, "Specify a number of threads for quicker execution time (default 1)") do |concurrency|
    @concurrency = concurrency
  end
end

option_parser.parse!

@project ||= 'gitlab-org/gitlab'
@concurrency ||= 1

def write_issues(issues)
  issues.each do |issue|
    artifact = Artifact.new(
      gitlab_id: issue.id,
      iid: issue.iid,
      title: issue.title,
      body: issue.description,
      web_url: issue.web_url
    )

    if issue.epic
      artifact.parent_id = issue.epic.id
      artifact.parent_iid = issue.epic.iid
      artifact.parent_type = 'Epic'
    end

    artifact.save
  end
end

issue_count = 0
issue_ids = []

# Create a set of threads from the initial pages array, the size of the concurrency
# value.
threads = @concurrency.times.map do |n|
  Thread.new do
    current_page = n + 1

    loop do
      # Grab the issues and write them
      issues = Gitlab.issues(@project, page: current_page, per_page: 100, sort: 'asc', order_by: 'created_at', confidential: false)
      write_issues(issues)

      issue_count += issues.size
      issue_ids << issues.map(&:iid)

      puts "Wrote #{issues.size} issues from page #{current_page}" unless issues.size == 0

      # If the number is less than the per_page, don't continue
      if issues.size == 100
        current_page += @concurrency
      else
        break
      end
    end
  end
end

threads.each do |thread|
  thread.join
end

puts "#{issue_count} issues created"

